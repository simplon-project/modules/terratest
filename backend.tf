terraform {
  backend "azurerm" {
    resource_group_name  = "Gregory_M_Projet_Final"
    storage_account_name = "backend10jun2402h30"
    container_name       = "tfstate10jun2402h30"
    key                  = "terraform.tfstate"
  }
}