# Prérequis:
# export TF_VAR_subscription_id="id-de-subscription"
# export TF_VAR_tenant_id="id-de-tenant"

terraform {
  required_version = ">= 1.0.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.68"
    }
  }
}

variable "subscription_id" {
  description = "The subscription_id where the resources will be deployed"
  type        = string
}
variable "tenant_id" {
  description = "The tenant_id where the resources will be deployed"
  type        = string
}

provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
  tenant_id       = var.tenant_id
}

module "backend" {
  source               = "git::git@gitlab.com:simplon-project/modules/backend.git?ref=v1.0.4"
  resource_group_name  = "Gregory_M_Projet_Final"
  storage_account_name = "backend"
  project              = "Final"
  Owner                = "Gregory_M"
  allowed_ips          = ["194.147.172.6", "91.163.152.102", "82.126.234.200"]
}


output "storage_account_name" {
  value = module.backend.storage_account_name
}

output "storage_container_name" {
  value = module.backend.storage_container_name
}

output "storage_account_key" {
  value     = module.backend.storage_account_key
  sensitive = true
}