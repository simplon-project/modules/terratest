# variables communes
variable "subscription_id" {
  description = "ID de l'abonnement Azure"
  type        = string
}
variable "tenant_id" {
  description = "ID du locataire Azure"
  type        = string
}
# variable "client_id" {
#   description = "ID du client Azure"
#   type        = string
# }
# variable "client_secret" {
#   description = "Secret du client Azure"
#   type        = string
# }
variable "location" {
  description = "nom de la région"
  type        = string
}
variable "resource_group_name" {
  description = "nom du groupe de ressources"
  type        = string
}
variable "project" {
  description = "Le nom du projet"
  type        = string
}
variable "owner" {
  description = "Le nom du propriétaire"
  type        = string
}
variable "allowed_ips" {
  description = "liste des adresses IP autorisées"
  type        = list(string)
}
variable "storage_account_name_backend" {
  description = "nom du compte de stockage pour le backend"
  type        = string
}
variable "azurerm_key_vault_secret_properties" {
  description = "Les propriétés du secret du Key Vault"
  type = object({
    backend_key_name         = string
    backend_key_content_type = string
  })
}
# propriétés du compte de stockage
variable "storage_account_name" {
  description = "nom du compte de stockage"
  type        = string
}
variable "storage_account_properties" {
  description = "Les propriétés du compte de stockage"
  type = object({
    account_kind                    = string
    account_tier                    = string
    account_replication_type        = string
    min_tls_version                 = string
    access_tier                     = string
    enable_https_traffic_only       = bool
    allow_nested_items_to_be_public = bool
    shared_access_key_enabled       = bool
    public_network_access_enabled   = bool
    is_hns_enabled                  = bool
    sftp_enabled                    = bool
    network_rules_properties = object({
      default_action             = string
      ip_rules                   = list(string)
      bypass                     = list(string)
      virtual_network_subnet_ids = list(string)
      private_link_access = object({
        endpoint_resource_id = string
        endpoint_tenant_id   = string
      })
    })
  })
}
variable "container_properties" {
  description = "Les propriétés du conteneur. 'container_access_type' peut être 'blob', 'container', 'private' ou 'off'."
  type = object({
    container_names       = list(string)
    container_access_type = string
  })
}
variable "local_user_properties" {
  description = "Les propriétés de l'utilisateur local"
  type = object({
    local_user_name      = string
    storage_account_id   = optional(string)
    ssh_key_enabled      = bool
    ssh_password_enabled = bool
    home_directory       = string
    permission_scope = object({
      permissions = object({
        read   = bool
        list   = bool
        delete = bool
        create = bool
        write  = bool
      })
      service = string
    })
  })
}
# propriétés du réseau virtuel
variable "vnet_location" {
  description = "nom de la région"
  type        = string
}
variable "vnets" {
  description = "Liste des VNet à créer, chaque VNet étant un objet avec un nom, un bloc CIDR et une liste de sous-réseaux"
  type = list(object({
    name = string
    cidr = string
    subnets = list(object({
      name = string
      cidr = string
    }))
  }))
}
variable "dns_servers_vnet" {
  description = "Liste des serveurs DNS à utiliser pour le réseau virtuel"
  type        = list(string)
  default     = []
}
variable "create_nsg" {
  description = "Indique si un NSG doit être créé ou non"
  type        = bool
  default     = true
}
variable "subnet_nsg_name" {
  description = "Nom du groupe de sécurité du sous-réseau"
  type        = string
  default     = "subnet-nsg"
}

# # propriétés de la passerelle d'application

# variable "app_gw_properties" {
#   description = "Les propriétés de la passerelle d'application"
#   type = list(object({
#     # Informations générales
#     gw_name             = string
#     location            = string
#     resource_group_name = string

#     # Configuration de l'IP publique
#     domain_name_label = string
#     allocation_method = string
#     sku               = string

#     # Configuration de la passerelle d'application
#     sku_name     = string
#     sku_tier     = string
#     sku_capacity = number
#     subnet_id    = list(string)

#     # Configuration du port frontal
#     frontend_port_name             = string
#     frontend_port                  = number
#     frontend_ip_configuration_name = string

#     # Configuration du pool d'adresses de backend
#     backend_address_pool_name = string

#     # Configuration des paramètres HTTP de backend
#     backend_http_settings_name     = string
#     cookie_based_affinity          = string
#     backend_http_settings_port     = number
#     backend_http_settings_protocol = string
#     request_timeout                = number

#     # Configuration de la sonde
#     probe_name          = string
#     probe_protocol      = string
#     probe_host          = string
#     probe_path          = string
#     probe_timeout       = number
#     probe_interval      = number
#     unhealthy_threshold = number

#     # Configuration de l'écouteur HTTP
#     http_listener_name     = string
#     http_listener_protocol = string

#     # Configuration de la redirection
#     redirect_configuration_name = string
#     redirect_type               = string
#     target_listener_name        = string
#     include_path                = bool
#     include_query_string        = bool

#     # Configuration de la règle de routage de requête
#     request_routing_rule_name = string
#     rule_type                 = string

#     log_analytics_workspace_name = string

#     ssl_profile = list(object({
#       name                                 = string
#       trusted_client_certificate_names     = list(string)
#       verify_client_cert_issuer_dn         = bool
#       verify_client_certificate_revocation = string
#     }))
#     ssl_policy = list(object({
#       # disabled_protocols   = list(string)
#       policy_type          = string
#       policy_name          = string
#       cipher_suites        = list(string)
#       min_protocol_version = string
#     }))
#     trusted_root_certificate = list(object({
#       name = string
#       data = string
#     }))
#     enable_http2 = bool
#   }))
# }

# variable "ip_public_app_gw" {
#   description = "Public IP for the Application Gateway"

#   type = object({
#     name                = string
#     resource_group_name = string
#     location            = string
#     sku                 = string
#     domain_name_label   = string
#     allocation_method   = string
#   })
# }
