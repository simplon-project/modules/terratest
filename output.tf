output "client_config" {
  value = {
    subscription_id = data.azurerm_client_config.current.subscription_id
    tenant_id       = data.azurerm_client_config.current.tenant_id
    client_id       = data.azurerm_client_config.current.client_id
    object_id       = data.azurerm_client_config.current.object_id
  }
  description = "Les attributs de la configuration du client Azure"
  sensitive   = true
}
output "key_vault" {
  value = {
    backend_key = azurerm_key_vault_secret.backend_key.value
    name        = module.azure-key-vault.azurerm_key_vault_name
    uri         = module.azure-key-vault.azurerm_key_vault_uri
    key         = module.azure-key-vault.azurerm_key_vault_key
  }
  description = "Les attributs du Key Vault"
  sensitive   = true
}
output "storage_account" {
  description = "Les attributs du compte de stockage"
  value = {
    resource_group_name                = module.storage_account.resource_group_name
    location                           = module.storage_account.location
    object_id                          = module.storage_account.object_id
    storage_account_id                 = module.storage_account.storage_account_id
    storage_account_name               = module.storage_account.storage_account_name
    storage_account_primary_access_key = module.storage_account.storage_account_primary_access_key
    storage_account_container_names    = [for c in module.storage_account.storage_account_container_names : c]
    local_user_name                    = module.storage_account.local_user_name
    local_user_id                      = module.storage_account.local_user_id
    local_user_ssh_authorized_key      = module.storage_account.local_user_ssh_authorized_key
    local_user_permission_scope        = module.storage_account.local_user_permission_scope
  }
  sensitive = true
}
output "vnets" {
  description = "Les attributs du réseau virtuel"
  value = {
    virtual_network_names          = module.vnets.virtual_network_names
    virtual_network_address_spaces = module.vnets.virtual_network_address_spaces
    subnet_address_prefixes        = module.vnets.subnet_address_prefixes
  }
  sensitive = true
}
# output "properties_app_gw" {
#   description = "Les attributs de la passerelle d'application"
#   value = {
#     app_gateway_name                                         = module.app_gateway.app_gateway_name
#     app_gateway_resource_group_name                          = module.app_gateway.app_gateway_resource_group_name
#     app_gateway_location                                     = module.app_gateway.app_gateway_location
#     app_gateway_sku_name                                     = module.app_gateway.app_gateway_sku_name
#     app_gateway_sku_tier                                     = module.app_gateway.app_gateway_sku_tier
#     app_gateway_sku_capacity                                 = module.app_gateway.app_gateway_sku_capacity
#     app_gateway_gateway_ip_configuration_name                = module.app_gateway.app_gateway_gateway_ip_configuration_name
#     app_gateway_gateway_ip_configuration_subnet_id           = module.app_gateway.app_gateway_gateway_ip_configuration_subnet_id
#     app_gateway_frontend_port_name                           = module.app_gateway.app_gateway_frontend_port_name
#     app_gateway_frontend_port_port                           = module.app_gateway.app_gateway_frontend_port_port
#     app_gateway_frontend_ip_configuration_name               = module.app_gateway.app_gateway_frontend_ip_configuration_name
#     app_gateway_backend_address_pool_name                    = module.app_gateway.app_gateway_backend_address_pool_name
#     app_gateway_backend_http_settings_name                   = module.app_gateway.app_gateway_backend_http_settings_name
#     app_gateway_backend_http_settings_cookie_based_affinity  = module.app_gateway.app_gateway_backend_http_settings_cookie_based_affinity
#     app_gateway_backend_http_settings_port                   = module.app_gateway.app_gateway_backend_http_settings_port
#     app_gateway_backend_http_settings_protocol               = module.app_gateway.app_gateway_backend_http_settings_protocol
#     app_gateway_backend_http_settings_request_timeout        = module.app_gateway.app_gateway_backend_http_settings_request_timeout
#     app_gateway_probe_name                                   = module.app_gateway.app_gateway_probe_name
#     app_gateway_probe_protocol                               = module.app_gateway.app_gateway_probe_protocol
#     app_gateway_probe_host                                   = module.app_gateway.app_gateway_probe_host
#     app_gateway_probe_path                                   = module.app_gateway.app_gateway_probe_path
#     app_gateway_probe_timeout                                = module.app_gateway.app_gateway_probe_timeout
#     app_gateway_probe_interval                               = module.app_gateway.app_gateway_probe_interval
#     app_gateway_probe_unhealthy_threshold                    = module.app_gateway.app_gateway_probe_unhealthy_threshold
#     app_gateway_http_listener_name                           = module.app_gateway.app_gateway_http_listener_name
#     app_gateway_http_listener_frontend_ip_configuration_name = module.app_gateway.app_gateway_http_listener_frontend_ip_configuration_name
#     app_gateway_http_listener_frontend_port_name             = module.app_gateway.app_gateway_http_listener_frontend_port_name
#     app_gateway_http_listener_protocol                       = module.app_gateway.app_gateway_http_listener_protocol
#     app_gateway_redirect_configuration_name                  = module.app_gateway.app_gateway_redirect_configuration_name
#     app_gateway_redirect_type                                = module.app_gateway.app_gateway_redirect_type
#     app_gateway_redirect_target_listener_name                = module.app_gateway.app_gateway_redirect_target_listener_name
#     app_gateway_redirect_include_path                        = module.app_gateway.app_gateway_redirect_include_path
#     app_gateway_redirect_include_query_string                = module.app_gateway.app_gateway_redirect_include_query_string
#   }
#   sensitive = true
# }
# output "properties_public_ip" {
#   description = "Les attributs de l'IP publique de l'app gateway"
#   value = {
#     public_ip_id      = module.app_gateway.public_ip_id
#     public_ip_address = module.app_gateway.public_ip_address
#     public_ip_dns     = module.app_gateway.public_ip_dns
#   }
#   sensitive = true
# }
# output "properties_v2_app_gw" {
#   description = "Les attributs de la passerelle d'application"
#   value = {
#     app_gateway_dns_name = module.app_gateway.app_gateway_dns_name
#     app_gateway_url      = module.app_gateway.app_gateway_url
#   }
# }
