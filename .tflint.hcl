#tflint configuration file

# activation du plugin azurerm
plugin "azurerm" {
    enabled = true
    version = "0.26.0"
    source  = "github.com/terraform-linters/tflint-ruleset-azurerm"
}
# activation des règles de vérification pour azurerm
rule azurerm_storage_account_invalid_access_tier {
  enabled = true
}
rule azurerm_storage_account_invalid_account_kind {
  enabled = true
}

# activation du plugin terraform
plugin "terraform" {
    enabled = true
    version = "0.7.0"
    source  = "github.com/terraform-linters/tflint-ruleset-terraform"
}

# activation des règles de vérification
rule "terraform_standard_module_structure" {
  enabled = false
}
rule "terraform_required_providers" {
  enabled = true
}
rule "terraform_module_pinned_source" {
  enabled = false
}
rule "terraform_deprecated_interpolation" {
  enabled = false
}
rule "terraform_unused_declarations" {
  enabled = false
}
rule "terraform_typed_variables" {
  enabled = true
}
rule "terraform_naming_convention" {
  enabled = false
}
rule "terraform_comment_syntax" {
  enabled = true
}