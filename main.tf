# Donnée de configuration du client Azure utilisé par Terraform
data "azurerm_client_config" "current" {}

# Donnée du compte de stockage pour stocker le fichier d'état Terraform
data "azurerm_storage_account" "tfstate" {
  name                = var.storage_account_name_backend
  resource_group_name = var.resource_group_name
}

module "azure-key-vault" {
  source                   = "git::git@gitlab.com:simplon-project/modules/azure-key-vault.git"
  location                 = var.location
  resource_group_name      = var.resource_group_name
  project                  = var.project
  owner                    = var.owner
  purge_protection_enabled = false
  allowed_ips              = var.allowed_ips
}

resource "azurerm_key_vault_secret" "backend_key" {
  name            = var.azurerm_key_vault_secret_properties.backend_key_name
  value           = data.azurerm_storage_account.tfstate.primary_access_key
  key_vault_id    = module.azure-key-vault.azurerm_key_vault_id
  content_type    = var.azurerm_key_vault_secret_properties.backend_key_content_type
  expiration_date = timeadd(timestamp(), "8760h")

  depends_on = [module.azure-key-vault]
}

module "storage_account" {
  source                     = "git::git@gitlab.com:simplon-project/modules/storage_account.git"
  resource_group_name        = var.resource_group_name
  storage_account_name       = var.storage_account_name
  storage_account_properties = var.storage_account_properties
  container_properties       = var.container_properties
  local_user_properties      = var.local_user_properties
  project                    = var.project
  owner                      = var.owner
}

module "vnets" {
  source              = "git@gitlab.com:simplon-project/modules/virtual_network_v2.git"
  resource_group_name = var.resource_group_name
  vnet_location       = var.vnet_location
  vnets               = var.vnets
  dns_servers_vnet    = var.dns_servers_vnet
  create_nsg          = var.create_nsg
  subnet_nsg_name     = var.subnet_nsg_name
  project             = var.project
  owner               = var.owner
}

# module "app_gateway" {
#   source              = "git::git@gitlab.com:simplon-project/modules/app_gateway.git"
#   resource_group_name = var.resource_group_name
#   ip_public_app_gw    = var.ip_public_app_gw
#   app_gw_properties   = var.app_gw_properties
#   subnet_id           = module.vnets.subnet_ids["snet1-appgw"]
#   # depends_on        = [module.vnets]
# }


